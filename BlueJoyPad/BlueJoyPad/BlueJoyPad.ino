
#include <Arduino.h>
#include <stdarg.h>
#include "common.h"
#include "config.h"
#include "utils.h"
#include "SerialProtocol.h"

/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/
#define IDX_UP      14
#define IDX_DOWN    15
#define IDX_LEFT    13
#define IDX_RIGHT   12

// 0.5us  per count
// 0.01ms per 20
#define OCR_VAL       22000     // 11ms

#define STATUS_NONE     0
#define STATUS_LOW      1
#define STATUS_HIGH     2
#define STATUS_DONE     0x80

/*
*****************************************************************************************
* MACROS
*****************************************************************************************
*/
#define CLR(x, y)               (x &= (~(1 << (y))))
#define SET(x, y)               (x |= (1 << (y)))
#define isSet(x, y)             (x & (1 << (y)))

/*
*****************************************************************************************
* VARIABLES
*****************************************************************************************
*/
volatile u16 mJoyBits;
volatile u8  mStatus = STATUS_NONE;
volatile u16 mStart = 0;
volatile u16 mDiff = 0;
volatile u16 mCnt = 0;

u8 tblPortD[] = {
    5,
    7,
    0,
    6,
    8,
    1,
};

u8 tblPortB[] = {
    2,
    3,
    4
};




/*
*****************************************************************************************
* setup
*****************************************************************************************
*/
void setup()
{
    pinMode(2, INPUT);
    pinMode(3, INPUT);
    pinMode(4, INPUT);
    pinMode(5, INPUT);
    pinMode(6, INPUT);
    pinMode(7, INPUT);
    pinMode(8, INPUT);
    pinMode(9, INPUT);
    pinMode(10, INPUT);
    pinMode(11, INPUT);
    pinMode(12, INPUT);
    pinMode(A4, INPUT);
    pinMode(A5, INPUT);
    pinMode(A6, INPUT);
    pinMode(A3, OUTPUT);
    Serial.begin(SERIAL_BPS);
//    LOG(F("---- BlueJoyPad ---- \n"));

    // TIMER1 for motor step
    TIMSK1 &= ~BV(OCIE1A);          // disable Timer1 interrupt
    TCCR1A = 0;                     // Timer1 normal mode 0, OCxA,B outputs disconnected
    TCCR1B = BV(CS11);              // Prescaler=8, => 2,000,000 Hz
    TCNT1  = 0;

    PCMSK1 = BV(4);                 // enable PC4
    PCICR  = BV(1);                 // enable PCINT1
    while (mStatus == STATUS_NONE);
//    LOG(F("---- Start ---- : %d\n"), mDiff);
}

//            x    y              btn
// 0xFD 0x06 0x00 0x00 0x00 0x00 0x55 0xAA
void sendPacket(u16 bits) {
    u8 packet[8];

    packet[0] = 0xfd;
    packet[1] = 0x06;
    packet[2] = isSet(bits, IDX_LEFT) ? -127 : (isSet(bits, IDX_RIGHT) ?  127 : 0);
    packet[3] = isSet(bits, IDX_UP)   ?  127 : (isSet(bits, IDX_DOWN)  ? -127 : 0);
    packet[4] = 0;
    packet[5] = 0;
    packet[6] = (bits & 0xff);
    packet[7] = (bits & 0xff) >> 8;
    Serial.write(packet, 8);
}

/*
*****************************************************************************************
* loop
*****************************************************************************************
*/
void loop()
{
    u8  pd = ~(PIND >> 2) & 0x3b;
    u8  pb = ~PINB & 0x0f;
    u16 btn;

    if (mStatus & STATUS_DONE) {
        btn = 0;

        for (u8 i = 0; i < sizeof(tblPortD); i++) {
            if (BV(i) & pd) {
                btn |= BV(tblPortD[i] - 1);
            }
        }

        for (u8 i = 0; i < sizeof(tblPortB); i++) {
            if (BV(i) & pb) {
                btn |= BV(tblPortB[i] - 1);
            }
        }

        mJoyBits |= btn;
        //LOG(F("%8d PD:%02x, PB:%02x, MASK:%04x\n"), mCnt, pd, pb, mJoyBits);
        sendPacket(mJoyBits);
        mStatus &= ~STATUS_DONE;
    }
    //LOG(F("PD:%02x, PB:%02x, A4:%04d, A5:%04d, A6:%04d\n"), pd, pb, analogRead(4), analogRead(5), analogRead(6));
}

/*
*****************************************************************************************
* ISR for TIMER1
*****************************************************************************************
*/
ISR(TIMER1_COMPA_vect)
{
    u16 adc;

    TCNT1 = 0;
    mCnt++;
    switch (mStatus & 0x0f) {
        case STATUS_LOW:
            if (mCnt > 1000) {
                OCR1A = mDiff - 18;                 // 18 * 0.5us = 9us shift
                mCnt = 0;
            } else {
                OCR1A = mDiff;
            }
            digitalWrite(A3, LOW);
            mJoyBits = 0;
            if (analogRead(6) < 1000) {
                adc = analogRead(4);
                if (800 < adc && adc < 1000)
                    SET(mJoyBits, IDX_DOWN);
                adc = analogRead(5);
                if (800 < adc && adc < 1000)
                    SET(mJoyBits, IDX_RIGHT);
            }
            mStatus = STATUS_HIGH;
            break;

        case STATUS_HIGH:
            OCR1A = mDiff;
            digitalWrite(A3, HIGH);
            if (analogRead(4) < 1000)
                SET(mJoyBits, IDX_UP);
            if (analogRead(5) < 1000)
                SET(mJoyBits, IDX_LEFT);
            mStatus = STATUS_DONE | STATUS_LOW;

            break;
    }
}

ISR(PCINT1_vect)
{
    u8  pins = PINC;

    if ((pins & BV(4)) == 0) {
        mStart = TCNT1;
    } else if ((pins & BV(4)) && mStart > 0) {
        mDiff = TCNT1 - mStart - 10;
        OCR1A = mDiff;
        TCNT1  = 0;

        while (TCNT1 < (mDiff >> 2));   // trim 1/4 period
        TCNT1  = 0;
        TIMSK1 |= BV(OCIE1A);           // Enable Timer1 interrupt

        PCMSK1 &= ~BV(4);
        PCICR  &= ~BV(1);

        mStatus = STATUS_LOW;
    }
}

